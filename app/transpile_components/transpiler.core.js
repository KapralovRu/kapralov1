/**
 * Это анонимная самовызывающая функция (вызов происходит на третьей от низа строке файла). Данная функция создается, 
 * чтобы убрать содержимое нашего скрипта из глобальной области видимости (например, чтобы другие наши скрипты и скрипты 
 * сторонних разработчиков случайно не запустили или сломали наш код). Использование такой конструкции с анонимной функцией
 * объясняется тем, что в браузерном JS нет возможности подключать напрямую в JS код другие модули на JS коде. Только через 
 * тег script. Это ограничение можно обойти с помощью AJAX запросов. Но для этого нам нужен сервер. Единственный  параметр функции - 
 * (window.trans || (window.trans = {})). С этим параметром функция  сама себя запускает в изолированной области видимости (scope) 
 * переменных. Переменная trans  является чем-то вроде пространства имен namespace, которая позволяет избежать  глобальных 
 * переменных и позволит в дальнейшем передавать данные между скриптами.  Аргумент (window.trans || (window.trans = {})) означает, 
 * что мы либо передадим в функцию  уже существующий объект window.trans, либо, если его нет, то создадим и передадим 
 * новый объект (window.trans = {}). В trans записываются функции, которые будут использовать в других файлах-скриптах (помимо данного)
 * @param {Object} trans - namespace глобальный объект для обмена данными между скриптами
 */
(function (trans) {
  trans.lexTableSrc = './config/lex_table.json'

  /**
   * Функция, которая запускается после того, как загружается вся страница. Данная функция считывает все script теги на 
   * странице, пропускает их текст через транспайлер и на основе кода, полученного из транспайлера делает функцию (чтобы 
   * получить изолированную область видимости) и запускает её
   * @memberof trans
   */
  function start() {    
    var inputCode = '';
    // Получаем все элементы script на нашей странице, у которых есть атрибут type="text/typescript". 
    // По умолчанию, браузер не запускает такие скрипты, но они есть в DOM дереве
    var typeScript = document.querySelectorAll('script[type="text/groovy"]');
    // Перебираем все элементы со скриптами
    for (var i = 0; i < typeScript.length; i++) { 
      if (typeScript[i].src) { 
        // Если у элемента есть атрибут src, то этот скрипт надо скачать с сервера
        inputCode += getScriptByXHR(typeScript[i]);
      } else { 
        // Иначе просто копируем текст изнутри тега скрипта
        inputCode += getScriptByText(typeScript[i]);
      }    
    }
    console.log(inputCode);
    // Запускаем трансляцию кода из исходного
    var generatedCode = transpileStart(inputCode); 
    // Создаем функцию на основе сгенерированного кода (изолируем scope)
    // var codeFunc = new Function('', generatedCode);
    // Запускаем функцию в текущем контекте (без этого может некорректно работать)
    // codeFunc.apply(this); 
    
    /**
     * Функция чтения содержимого тега скрипта и добавления к нему ";"
     * @param {Element} scriptElem 
     */
    function getScriptByText(scriptElem) {
      return scriptElem.textContent ;
    }
    
    /**
     * Функция чтения файла скрипта с сервера 
     * @param {Element} scriptElem 
     */
    function getScriptByXHR(scriptElem) {
      // Получаем файл скрипта с сервера через AJAX запрос
      var response = getFileByXHR(scriptElem.src); 
      // Проверяем не является ли полученный ответ объектом ошибки
      if (response instanceof Error) { 
        // Если это объект ошибки, то просто возвращаем строку с тектом ошибки
        return "\n'noscript'"; 
      } else { 
        // Иначе возвращаем текст содержимого файла плюс ";"
        return response ;
        // + ';'; 
      }
    }
  }
  trans.start = start; // Сохраняем ссылку на функцию start в namespace объекте trans
  
  /**
   * Функция, которая с помощью AJAX запроса через XMLHttpRequest получает файлы с сервера
   * @param {string} urlName - адрес файла, который необходимо получить с сервера
   */
  function getFileByXHR(urlName) {
    // Создаём новый объект XMLHttpRequest
    var xhr = new XMLHttpRequest();
    // Конфигурируем его: GET-запрос на URL urlName
    xhr.open('GET', urlName, false); 
    // Отсылаем запрос
    xhr.send(); 
    // Если код ответа сервера не 200, то это ошибка
    if (xhr.status != 200) {
      // обработать ошибку
      console.error("Can't get file: " + urlName);
      // Возвращаем новый объект ошибки с указанием текста ошибки
      return new Error("Can't get file: " + urlName);
    } else {
      return xhr.responseText; // responseText -- текст ответа.
    }
  }
  trans.getFileByXHR = getFileByXHR;

  /**
   * Функция запуска трансляции. Внутри создается экземпляр траспайлера от Transpiler, который и будет содержать в себе
   * код для трансляции из входного языка в JS код
   * @param {string} inputCode - код на входном языке
   */  
  function transpileStart(inputCode) {
    // Создаем от конструктора (класса) Transpiler новый объект, если он еще не существует
    var transpiler = trans.transpiler = trans.transpiler || new Transpiler(trans.lexTableSrc);
    // Запускаем на объекте transpiler запуск процесса трансляции
    var resultCode = transpiler.transpile(inputCode);
    return resultCode; // Возвращаем итоговый код на JS
  }
  trans.transpileStart = transpileStart;
  
  /**
   * Функция конструктор (в JS нет классов, есть функции конструкторы - аналог классов из C++). Этот конструктор
   * создает экземпляры нашего транспайлера (в котором есть пока что только лексер). В экземпляр сохраняем массив 
   * символов, таблицу лексем, массив лексем и массив ошибок. Для создания методов класса, в JS используется
   * объект prototype, который определяется на функции конструктора. В объект prototype записываются ссылки на методы,
   * которые будут методами объекта, создаваемого от функции конструктора (класса)
   * @param {string} lexTablePath - путь до JSON файла таблицы лексем
   */
  function Transpiler(lexTablePath) {
    // this это ссылка на текущий объект, который будет создан от конструктора Transpiler
    this.arrayOfSymbols = {}; // массив символов, куда будут помещаться ссылки на массивы констант и идентификаторов
    this.lexTable = trans.getJSON(lexTablePath, this.arrayOfSymbols); // Таблица лексем, отпарсенная в JS-объект
    this.lexArray = []; // Массив найденных лексем
    this.errors = []; // Массив ошибок
  }
  trans.Transpiler = Transpiler; // Сохраняем ссылку на конструктор Transpiler в namespace объект trans
  trans.TransProto = trans.Transpiler.prototype; // Делаем алиас на прототип и сохраняем его в trans (делаем его короче)
  
  /**
   * Функция запуска процесса трансляции входного кода в код на JS.
   * Функция transpile это первая функция в прототипе конструктора Transpiler. Теперь мы можем запускать эту функцию на
   * объектах, созданных от Transpiler. Например: transpiler.transpile(inputCode), что было сделано в коде выше.
   * @param {string} inputCode - код на входном языке
   */
  trans.TransProto.transpile = function(inputCode) {
    var lexArray = this.lexer(inputCode); // Запускаем лексер
    console.log(lexArray);
    // Здесь пока затычка. Надо дописать функцию, чтобы она транслировала код, а не возвращала входной код
    return inputCode;
  }
  
  /**
   * Функция чтения и парсинга JSON c сервера
   * @param {string} fileName - Имя JSON файла
   * @param {Array} arrayOfSymbols - массив символов, куда будут помещаться ссылки на массивы констант и идентификаторов
   * @return {Object}
   */
  function getJSON(fileName, arrayOfSymbols) {
    var textJSON = trans.getFileByXHR(fileName);
    if (textJSON instanceof Error) throw textJSON;
    var jsonObj;
    // заменяем \ на двойные \\ по всей строке json, за исключением кавычек "
    textJSON = textJSON.replace(/\\([^"])/gi, "\\\\$1");
    try {
      jsonObj = JSON.parse(textJSON, function(key, value) {
        // Создаем регулярки и добавляем им символ начала строки
        if (key == 'regex') return new RegExp("^" + value, 'i');
        // Сохраняем в поле link ссылку на объект с именем value - так создаем таблицы идентификаторов и констант
        if (key == 'link') {
          if (!(value in arrayOfSymbols)) arrayOfSymbols[value] = {};
          return arrayOfSymbols[value] = value;
        }
        return value;
      });
    } catch (error) {
      throw error;
    }
    return jsonObj;
  }
  trans.getJSON = getJSON;
  
  // Подписка на событие загрузки страницы. Функция start запускается только после полного рендеринга страницы
  document.addEventListener('DOMContentLoaded', start);

})(window.trans || (window.trans = {})); // Функция вызывает саму себя с указанным параметром